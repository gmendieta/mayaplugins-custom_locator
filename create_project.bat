

if exist project_2022 (
    goto exit
)

mkdir project_2022
cd project_2022
cmake -G "Visual Studio 15 2017 Win64" -DMAYA_VERSION=2022 ..

:exit
pause