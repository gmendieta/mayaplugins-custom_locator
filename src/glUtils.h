#ifndef _GL_UTILS_H_
#define _GL_UTILS_H_

#include <maya/MGL.h> // OpenGL
#include <maya/MUIDrawManager.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MQuaternion.h>
#include <vector>

extern std::vector<MPoint> quadArrowVertices;

inline void cube(float scale)
{
	float scaled = 0.5f * scale;
	float neg_scaled = -0.5f * scale;

	glBegin(GL_LINES);
		// Up Lines
		glVertex3f(neg_scaled, scaled, scaled);
		glVertex3f(scaled, scaled, scaled);

		glVertex3f(scaled, scaled, scaled);
		glVertex3f(scaled, scaled, neg_scaled);

		glVertex3f(scaled, scaled, neg_scaled);
		glVertex3f(neg_scaled, scaled, neg_scaled);

		glVertex3f(neg_scaled, scaled, neg_scaled);
		glVertex3f(neg_scaled, scaled, scaled);

		// Bottom Lines
		glVertex3f(neg_scaled, neg_scaled, scaled);
		glVertex3f(scaled, neg_scaled, scaled);

		glVertex3f(scaled, neg_scaled, scaled);
		glVertex3f(scaled, neg_scaled, neg_scaled);

		glVertex3f(scaled, neg_scaled, neg_scaled);
		glVertex3f(neg_scaled, neg_scaled, neg_scaled);

		glVertex3f(neg_scaled, neg_scaled, neg_scaled);
		glVertex3f(neg_scaled, neg_scaled, scaled);

		// Vertical Lines
		glVertex3f(neg_scaled, neg_scaled, scaled);
		glVertex3f(neg_scaled, scaled, scaled);

		glVertex3f(scaled, neg_scaled, scaled);
		glVertex3f(scaled, scaled, scaled);

		glVertex3f(scaled, neg_scaled, neg_scaled);
		glVertex3f(scaled, scaled, neg_scaled);

		glVertex3f(neg_scaled, neg_scaled, neg_scaled);
		glVertex3f(neg_scaled, scaled, neg_scaled);
	glEnd();
}

/**
* Draw a Quadruped Arrow pointing in the X axis
*/
inline void quadArrow(float scale)
{
	glBegin(GL_LINE_LOOP);
	for (auto it = quadArrowVertices.begin(); it != quadArrowVertices.end(); ++it)
	{
		glVertex3f((*it).x, scale * (*it).y, (*it).z);
	}
	glEnd();
}

/**
* Draw a Circle pointing in the X axis
*/
inline void circle(float scale, unsigned int res=16)
{
	glBegin(GL_LINE_LOOP); // Could also use GL_LINE_STRIP if we add the first vertex at the end
	for(unsigned int i = 0; i < res; ++i)
	{
		float angle = (i * 360.0f / (float)res) * M_PI / 180.0f;
		glVertex3f(0.0f, sin(angle) * scale, cos(angle) * scale);
	}
	glEnd();
}

/**
* Draw a Locator as the Maya one
*/
inline void locator(float scale)
{
	// The size has been compared with Maya Locator
	glBegin(GL_LINES);
	glVertex3f(-1.0f * scale, 0, 0);
	glVertex3f(1.0f * scale, 0, 0);
	glVertex3f(0,  1.0f * scale, 0);
	glVertex3f(0, -1.0f * scale, 0);
	glVertex3f(0, 0, 1.0f * scale);
	glVertex3f(0, 0, -1.0f * scale);
	glEnd();
}

/**
* Draw a Cone
*/
inline void cone(const float angle, const float scale, const unsigned resolution=16)
{
	glBegin(GL_LINE_LOOP);
	// Get the Radius based on Scale
	const float radius = tan(angle * M_PI / 180.0f) * scale;
	for(unsigned int i = 0; i < resolution; ++i)
	{
		const float radians = (i * 360.0f / static_cast<float>(resolution)) * M_PI / 180.0f;
		glVertex3f(scale, sin(radians) * radius, cos(radians) * radius);
	}
	glEnd();
	glBegin(GL_LINES);
	for(unsigned int i = 0; i < resolution; i+=2)
	{
		const float radians = (i * 360.0f / static_cast<float>(resolution)) * M_PI / 180.0f;
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(scale, sin(radians) * radius, cos(radians) * radius);
	}
	glEnd();
}

/**
* Draw a Disk
*/
inline void disk(const float inner, const float scale, const unsigned int resolution=16)
{
	float radians = 0;
	for (unsigned int i = 0; i < resolution; ++i)
	{
		glBegin(GL_POLYGON);
		auto sa = sin(radians);
		auto ca = cos(radians);
		glVertex3f(0.0f, sa * inner, ca * inner);
		glVertex3f(0.0f, sa * scale, ca * scale);
		radians = ((i + 1) * 360.0f / static_cast<float>(resolution)) * M_PI / 180.0f;
		sa = sin(radians);
		ca = cos(radians);
		glVertex3f(0.0f, sa * scale, ca * scale);
		glVertex3f(0.0f, sa * inner, ca * inner);
		glEnd();
	}
}


// Viewport 2.0

// This values are asigned for retrocompatibility
enum class DrawableType: short
{
	Unknown		= 255,
	Circle		= 0,
	Cube		= 2,
	Disk		= 4,
	Locator		= 5,
	QuadArrow	= 6,
	Cone		= 7
};

class Drawable
{
public:
	virtual ~Drawable() = default;
	virtual void draw(MHWRender::MUIDrawManager& drawManager) = 0;
	virtual void draw(MHWRender::MUIDrawManager& drawManager, const MMatrix &transform) = 0;
	virtual bool operator==(const Drawable &rhs) const = 0;
	void transformVertices(MPointArray &vertices, const MMatrix &matrix)
	{
		for (int i = 0; i < vertices.length(); ++i)
			vertices[i] *= matrix;
	}

	DrawableType getType() const
	{
		return type;
	}

protected:
	DrawableType type;
};

class Locator : public Drawable
{
	MPointArray m_vertices;
	float m_scale;
public:
	Locator(const float &scale):
		m_scale(scale)
	{
		type = DrawableType::Locator;
		initGeometry();
	}

	void setParameters(const float &scale)
	{
		const bool rebuild = m_scale != scale;

		if (rebuild)
		{
			m_scale = scale;
			initGeometry();
		}
	}

	void draw(MHWRender::MUIDrawManager& drawManager) override
	{
		drawManager.mesh(MHWRender::MUIDrawManager::kLines, m_vertices);
	}

	void draw(MHWRender::MUIDrawManager& drawManager, const MMatrix &transform) override
	{
		transformVertices(m_vertices, transform);
		draw(drawManager);
	}

	void initGeometry()
	{
		m_vertices.clear();

		m_vertices.append(-1.0f * m_scale, 0.0f, 0.0f);
		m_vertices.append(1.0f * m_scale, 0.0f, 0.0f);
		m_vertices.append(0.0f, 1.0f * m_scale, 0.0f);
		m_vertices.append(0.0f, -1.0f * m_scale, 0.0f);
		m_vertices.append(0.0f, 0.0f, 1.0f * m_scale);
		m_vertices.append(0.0f, 0.0f, -1.0f * m_scale);
	}

	bool operator==(const Drawable &rhs) const override
	{
		return false;
	}
};

class Circle : public Drawable
{
	MPointArray m_vertices;
	float m_scale;
	unsigned m_resolution;
public:
	Circle(const float &scale, unsigned resolution) :
		m_scale(scale),
		m_resolution(resolution)
	{
		type = DrawableType::Circle;

		initGeometry();
	}

	void setParameters(const float &scale, unsigned resolution)
	{
		const bool rebuild = m_scale != scale || m_resolution != resolution;

		if (rebuild)
		{
			m_scale = scale;
			m_resolution = resolution;

			initGeometry();
		}
	}

	void draw(MHWRender::MUIDrawManager& drawManager) override
	{
		drawManager.mesh(MHWRender::MUIDrawManager::kLines, m_vertices);
	}

	void draw(MHWRender::MUIDrawManager& drawManager, const MMatrix &transform) override
	{
		transformVertices(m_vertices, transform);
		draw(drawManager);
	}

	void initGeometry()
	{
		float last_angle = 0.0f;
		//  i<=resolution cause we want the entire circle
		for (unsigned int i = 1; i <= m_resolution; ++i)
		{
			const float angle = (i * 360.0f / static_cast<float>(m_resolution)) * M_PI / 180.0f;
			m_vertices.append(0.0f, sin(last_angle) * m_scale, cos(last_angle) * m_scale);
			m_vertices.append(0.0f, sin(angle) * m_scale, cos(angle) * m_scale);
			last_angle = angle;
		}
	}

	bool operator==(const Drawable &rhs) const override
	{
		return false;
	}
	
};

class QuadArrow : public Drawable
{
	MPointArray m_vertices;
	float m_scale;
public:
	QuadArrow(const float &scale):
		m_scale(scale)
	{
		type = DrawableType::QuadArrow;

		initGeometry();
	}

	void setParameters(const float &scale)
	{
		const bool rebuild = m_scale != scale;

		if (rebuild)
		{
			m_scale = scale;
			initGeometry();
		}
	}

	void draw(MHWRender::MUIDrawManager& drawManager) override
	{
		drawManager.mesh(MHWRender::MUIDrawManager::kLines, m_vertices);
	}

	void draw(MHWRender::MUIDrawManager& drawManager, const MMatrix &transform) override
	{
		transformVertices(m_vertices, transform);
		draw(drawManager);
	}

	void initGeometry()
	{
		m_vertices.clear();

		for (auto it = quadArrowVertices.begin(); it != quadArrowVertices.end() - 1; ++it)
		{
			m_vertices.append((*it).x, m_scale * (*it).y, m_scale * (*it).z);
			m_vertices.append((*(it + 1)).x, m_scale * (*(it + 1)).y, m_scale * (*(it + 1)).z);
		}
	}

	bool operator==(const Drawable &rhs) const override
	{
		return false;
	}
};

class Disk : public Drawable
{
	MPointArray m_vertices;
	float m_inner;
	float m_scale;
	unsigned m_resolution;
public:
	Disk(const float &inner, const float &scale, unsigned resolution):
		m_inner(inner),
		m_scale(scale),
		m_resolution(resolution)
	{
		type = DrawableType::Disk;

		initGeometry();
	}

	void setParameters(const float &inner, const float &scale, unsigned resolution)
	{
		const bool rebuild = m_inner != inner || m_scale != scale || m_resolution != resolution;

		if (rebuild)
		{
			m_inner = inner;
			m_scale = scale;
			m_resolution = resolution;

			initGeometry();
		}
	}

	void draw(MHWRender::MUIDrawManager& drawManager) override
	{
		drawManager.mesh(MHWRender::MUIDrawManager::kTriangles, m_vertices);
	}

	void draw(MHWRender::MUIDrawManager& drawManager, const MMatrix &transform) override
	{
		transformVertices(m_vertices, transform);
		draw(drawManager);
	}

	void initGeometry()
	{
		m_vertices.clear();

		float radians = 0;
		for (unsigned int i = 0; i < m_resolution; ++i)
		{
			float sa = sin(radians);
			float ca = cos(radians);
			MPoint p1 = MPoint(0.0f, sa * m_inner, ca * m_inner);
			MPoint p2 = MPoint(0.0f, sa * m_scale, ca * m_scale);
			radians = ((i + 1) * 360.0f / static_cast<float>(m_resolution)) * M_PI / 180.0f;
			sa = sin(radians);
			ca = cos(radians);
			MPoint p3 = MPoint(0.0f, sa * m_inner, ca * m_inner);
			MPoint p4 = MPoint(0.0f, sa * m_scale, ca * m_scale);
			// First Triangle
			m_vertices.append(p1);
			m_vertices.append(p3);
			m_vertices.append(p2);
			// Second Triangle
			m_vertices.append(p2);
			m_vertices.append(p3);
			m_vertices.append(p4);
		}
	}

	bool operator==(const Drawable &rhs) const override
	{
		return false;
	}
};


class Cone : public Drawable
{
	MPointArray m_vertices;
	float m_angle;
	float m_scale;
	unsigned m_resolution;
public:
	Cone(const float &angle, const float &scale, unsigned resolution = 16):
		m_angle(angle),
		m_scale(scale),
		m_resolution(resolution)
	{
		type = DrawableType::Cone;

		initGeometry();
	}

	void setParameters(const float &angle, const float &scale, unsigned resolution = 16)
	{
		const bool rebuild = m_angle != angle || m_scale != scale || m_resolution != resolution;

		if (rebuild)
		{
			m_angle = angle;
			m_scale = scale;
			m_resolution = resolution;

			initGeometry();
		}
	}

	void draw(MHWRender::MUIDrawManager& drawManager) override
	{
		drawManager.mesh(MHWRender::MUIDrawManager::kLines, m_vertices);
	}

	void draw(MHWRender::MUIDrawManager& drawManager, const MMatrix &transform) override
	{
		transformVertices(m_vertices, transform);
		draw(drawManager);
	}

	void initGeometry()
	{
		m_vertices.clear();

		float last_angle = 0.0f;
		const float radius = tan(m_angle * M_PI / 180.0) * m_scale;
		for (unsigned int i = 1; i <= m_resolution; ++i)
		{
			const float angle = (i * 360.0f / static_cast<float>(m_resolution)) * M_PI / 180.0f;
			m_vertices.append(m_scale, sin(last_angle) * radius, cos(last_angle) * radius);
			m_vertices.append(m_scale, sin(angle) * radius, cos(angle) * radius);
			last_angle = angle;
		}

		// Generate the lines from center to the Circle
		for (unsigned int i = 0; i < m_resolution; i += 2)
		{
			const float angle = (i * 360.0f / static_cast<float>(m_resolution)) * M_PI / 180.0f;
			m_vertices.append(0.0f, 0.0f, 0.0f);
			m_vertices.append(m_scale, sin(angle) * radius, cos(angle) * radius);
		}
	}

	bool operator==(const Drawable &rhs) const override
	{
		return false;
	}
};

class Cube : public Drawable
{
	MPointArray m_vertices;
	float m_scale;
public:
	Cube(const float scale):
		m_scale(scale)
	{
		type = DrawableType::Cube;

		initGeometry();
	}

	void setParameters(const float &scale)
	{
		const bool rebuild = m_scale != scale;

		if (rebuild)
		{
			m_scale = scale;
			initGeometry();
		}
	}

	void draw(MHWRender::MUIDrawManager& drawManager) override
	{
		drawManager.mesh(MHWRender::MUIDrawManager::kLines, m_vertices);
	}

	void draw(MHWRender::MUIDrawManager& drawManager, const MMatrix &transform) override
	{
		transformVertices(m_vertices, transform);
		draw(drawManager);
	}

	void initGeometry()
	{
		m_vertices.clear();

		const float scaled = 0.5f * m_scale;
		const float neg_scaled = -0.5f * m_scale;

		// Up Lines
		m_vertices.append(neg_scaled, scaled, scaled);
		m_vertices.append(scaled, scaled, scaled);

		m_vertices.append(scaled, scaled, scaled);
		m_vertices.append(scaled, scaled, neg_scaled);

		m_vertices.append(scaled, scaled, neg_scaled);
		m_vertices.append(neg_scaled, scaled, neg_scaled);

		m_vertices.append(neg_scaled, scaled, neg_scaled);
		m_vertices.append(neg_scaled, scaled, scaled);


		// Bottom Lines
		m_vertices.append(neg_scaled, neg_scaled, scaled);
		m_vertices.append(scaled, neg_scaled, scaled);

		m_vertices.append(scaled, neg_scaled, scaled);
		m_vertices.append(scaled, neg_scaled, neg_scaled);

		m_vertices.append(scaled, neg_scaled, neg_scaled);
		m_vertices.append(neg_scaled, neg_scaled, neg_scaled);

		m_vertices.append(neg_scaled, neg_scaled, neg_scaled);
		m_vertices.append(neg_scaled, neg_scaled, scaled);

		// Vertical Lines
		m_vertices.append(neg_scaled, neg_scaled, scaled);
		m_vertices.append(neg_scaled, scaled, scaled);

		m_vertices.append(scaled, neg_scaled, scaled);
		m_vertices.append(scaled, scaled, scaled);

		m_vertices.append(scaled, neg_scaled, neg_scaled);
		m_vertices.append(scaled, scaled, neg_scaled);

		m_vertices.append(neg_scaled, neg_scaled, neg_scaled);
		m_vertices.append(neg_scaled, scaled, neg_scaled);
	}

	bool operator==(const Drawable &rhs) const override
	{
		return false;
	}
};

#endif