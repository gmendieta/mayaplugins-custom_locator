#ifndef customLocatorDrawOverride_h
#define customLocatorDrawOverride_h

//Viewport 2.0
#include <maya/MPxDrawOverride.h>
#include <maya/MUserData.h>
#include <maya/MDrawContext.h>

#include "glUtils.h"

// Viewport 2.0
class CustomLocatorData : public MUserData
{
public:
	CustomLocatorData() : MUserData(false) {}
	virtual ~CustomLocatorData() {}

	bool drawIt;
	bool xRay;
	bool billboard;
	MColor color;
	float line_width;
	std::unique_ptr<Drawable> drawable;
};

class CustomLocatorDrawOverride : MHWRender::MPxDrawOverride
{
public:
	static MHWRender::MPxDrawOverride* creator(const MObject& ob);
	virtual ~CustomLocatorDrawOverride();
	MHWRender::DrawAPI supportedDrawAPIs() const override;
	bool isBounded(const MDagPath &obPath, const MDagPath &cameraPath) const override;
	MBoundingBox boundingBox(const MDagPath& obPath, const MDagPath& cameraPath) const override;
	virtual bool hasUIDrawables() const;
	MUserData* prepareForDraw(const MDagPath& obPath, const MDagPath& cameraPath, const MHWRender::MFrameContext& frameContext, MUserData* oldData) override;
	void addUIDrawables(const MDagPath& obPath, MHWRender::MUIDrawManager& drawManager, const MHWRender::MFrameContext& frameContext, const MUserData* data) override;
	static void draw(const MHWRender::MDrawContext& context, const MUserData* userData) {}

private:
	CustomLocatorDrawOverride(const MObject& ob);
};

#endif