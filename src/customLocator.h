#ifndef customLocator_h
#define customLocator_h

#include <memory>

#include <maya/MTypeId.h>
#include <maya/MPxLocatorNode.h>
#include <maya/MGlobal.h>
#include <maya/MMatrix.h>
#include <maya/MColor.h>

class CustomLocator: public MPxLocatorNode
{
	MColor selectedColor;

public:
	static MTypeId typeId;
	static MObject aDrawIt; // Bool to Draw or not Draw
	static MObject aXray;
	//static MObject aBackFaceCull; // Bool to Draw Back Faces
	static MObject aBillboard; // Billboard mode. Look to Camera
	//static MObject aWireframe;
	static MObject aLocatorShape; // Enum
	static MObject aScale;
	static MObject aLineWidth; // Float Line Width
	static MObject aAlpha; // Float Alpha
	static MObject aLeadColor; // Color when Lead
	static MObject aSelectedColor; // Color when Selected
	static MObject aUnselectedColor; // Color when Not Selected

	static MObject aUseDistanceColor; // Use Calculation of Alpha based on Distance from Camera
	static MObject aMaxDistance; // Max Distance from Camera
	static MObject aMinDistance; // Min Distance from Camera
	static MObject aDistanceAlpha; // Alpha on Distance

	static MObject aConeRadius;
	static MObject aDiskRadius;

	CustomLocator();
	virtual void postConstructor();
	virtual ~CustomLocator();

	static void* creator();
	static MStatus initialize();
	
	virtual MStatus compute(const MPlug& plug, MDataBlock& data);

	virtual void draw(M3dView&, const MDagPath&, M3dView::DisplayStyle, M3dView::DisplayStatus);
	virtual bool isBounded() const;
	virtual bool isTransparent() const;

	// Viewport 2.0 variables
	static MString drawDbClassification;
	static MString drawRegistrantId;
};

#endif