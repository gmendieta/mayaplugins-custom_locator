
#ifndef API_UTILS_H
#define API_UTILS_H

#include <string>
#include <algorithm> // min & max

using namespace std;

#include <maya/MGlobal.h>
#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/M3dView.h>
#include <maya/MDagPath.h>
#include <maya/MSelectionList.h>
#include <maya/MPoint.h>
#include <maya/MVector.h>
#include <maya/MMatrix.h>
#include <maya/MEulerRotation.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MArrayDataBuilder.h>

inline MStatus getRGBFromPlug(const MObject& ob, const MObject& attr, float& r, float& g, float& b)
{
	MPlug plugColor(ob, attr);
	plugColor.child(0).getValue(r);
	plugColor.child(1).getValue(g);
	plugColor.child(2).getValue(b);

	return MS::kSuccess;
}

inline MStatus getDagPathFromMObject(const MObject& ob, MDagPath& dagPath)
{
	MSelectionList selectionList;
	MStatus status = selectionList.add(ob);
	status = selectionList.getDagPath(0, dagPath);
	return status;
}

inline MStatus getCameraMatrixFromView3D(M3dView& view3d, MMatrix& cameraMatrix)
{
	MDagPath cameraPath;
	MStatus status = view3d.getCamera(cameraPath);
	cameraMatrix = cameraPath.inclusiveMatrix();
	return status;
}

inline float getDistanceFromCamera(const MMatrix &matrix, M3dView &view)
{
	const MVector position(matrix[3][0], matrix[3][1], matrix[3][2]);
	MMatrix cameraMatrix;
	getCameraMatrixFromView3D(view, cameraMatrix);
	MVector camPos(cameraMatrix[3][0], cameraMatrix[3][1], cameraMatrix[3][2]);
	// Return the Length
	return static_cast<float>((position - camPos).length());
}

inline float getDistanceFromCamera(const MMatrix &matrix, const MDagPath &cameraPath)
{
	const MVector position(matrix[3][0], matrix[3][1], matrix[3][2]);
	MMatrix cameraMatrix = cameraPath.inclusiveMatrix();
	const MVector camPos(cameraMatrix[3][0], cameraMatrix[3][1], cameraMatrix[3][2]);
	// Return the Length
	return static_cast<float>((position - camPos).length());
}

inline float getClampedLerp(const float& a, const float& b, const float& t, const float& min, const float& max)
{
	const float clamp = std::min(1.0f, std::max(0.0f, (t - min) / (max - min)));
	return (a + (b - a) * clamp);
}

inline void printInfo(string message)
{
	MGlobal::displayInfo(MString(message.c_str()));
}

inline void printWarning(string message)
{
	MGlobal::displayWarning(MString(message.c_str()));
}

inline void printMPoint(string header, const MPoint& mPoint)
{
	string message = header;
	message += "(" + to_string(mPoint.x) + "," + to_string(mPoint.y) + "," + to_string(mPoint.z) + ")";
	MGlobal::displayInfo(MString(message.c_str()));
}

inline void printMVector(string header, const MVector &mVec)
{
	string message = header;
	message += "(" + to_string(mVec.x) + "," + to_string(mVec.y) + "," + to_string(mVec.z) + ")";
	MGlobal::displayInfo(MString(message.c_str()));
}

inline void printMEulerRotation(string header, const MEulerRotation &mEuler)
{
	string message = header;
	message += "(" + to_string(mEuler.x) + "," + to_string(mEuler.y) + "," + to_string(mEuler.z) + ")";
	MGlobal::displayInfo(MString(message.c_str()));
}

inline void printMMatrix(string header, const MMatrix &mMatrix)
{
	string message = header + " [\n";
	message += "[[" + to_string(mMatrix[0][0]) + "],[" + to_string(mMatrix[0][1]) + "],[" + to_string(mMatrix[0][2]) + "] " + "],[" + to_string(mMatrix[0][3]) + "]\n";
	message += "[" + to_string(mMatrix[1][0]) + "],[" + to_string(mMatrix[1][1]) + "],[" + to_string(mMatrix[1][2]) + "] " + "],[" + to_string(mMatrix[1][3]) + "]\n";
	message += "[" + to_string(mMatrix[2][0]) + "],[" + to_string(mMatrix[2][1]) + "],[" + to_string(mMatrix[2][2]) + "] " + "],[" + to_string(mMatrix[2][3]) + "]\n";
	message += "[" + to_string(mMatrix[3][0]) + "],[" + to_string(mMatrix[3][1]) + "],[" + to_string(mMatrix[3][2]) + "] " + "],[" + to_string(mMatrix[3][3]) + "]]\n";
	MGlobal::displayInfo(MString(message.c_str()));
}

inline MStatus jumpToElement(MArrayDataHandle& hArray, unsigned int index)
{
	MStatus status = hArray.jumpToElement(index);
	// If the index does not exist
	if (MFAIL(status)) {
		MArrayDataBuilder builder = hArray.builder(&status);
		CHECK_MSTATUS_AND_RETURN_IT(status);
		// Add the default value at the index
		builder.addElement(index, &status);
		CHECK_MSTATUS_AND_RETURN_IT(status);
		status = hArray.set(builder);
		CHECK_MSTATUS_AND_RETURN_IT(status);
		status = hArray.jumpToElement(index);
		CHECK_MSTATUS_AND_RETURN_IT(status);
	}
	return status;
}

inline bool isPlugConnected(const MObject& mObject, const MObject& mPlug)
{
	MPlug plug(mObject, mPlug);
	return plug.isConnected();
}

template <typename T>
inline T getPlugValue(const MDagPath& mDagPath, const MObject& mPlug, const T default_value)
{
	MStatus status;
	const MObject mObject = mDagPath.node(&status);
	if (status)
	{
		MPlug plug(mObject, mPlug);
		if (!plug.isNull())
		{
			T value;
			if (plug.getValue(value))
			{
				return value;
			}
		}
	}
	return default_value;
}

#endif