#include "customLocatorDrawOverride.h"

#include <maya/MFnMatrixData.h>
#include <maya/MHWGeometryUtilities.h>

#include "customLocator.h"
#include "apiUtils.h"

// Viewport 2.0
MHWRender::MPxDrawOverride* CustomLocatorDrawOverride::creator(const MObject& ob)
{
	return new CustomLocatorDrawOverride(ob);
}

#ifdef MAYA_2018
CustomLocatorDrawOverride::CustomLocatorDrawOverride(const MObject& ob)
	: MHWRender::MPxDrawOverride(ob, CustomLocatorDrawOverride::draw, true)
{
	;
}
#else
CustomLocatorDrawOverride::CustomLocatorDrawOverride(const MObject& ob)
	: MHWRender::MPxDrawOverride(ob, CustomLocatorDrawOverride::draw)
{
	;
}
#endif

CustomLocatorDrawOverride::~CustomLocatorDrawOverride()
{
	;
}

MHWRender::DrawAPI CustomLocatorDrawOverride::supportedDrawAPIs() const
{
	return (MHWRender::kOpenGL | MHWRender::kDirectX11 | MHWRender::kOpenGLCoreProfile);
}

bool CustomLocatorDrawOverride::isBounded(const MDagPath &obPath, const MDagPath &cameraPath) const
{
	return false;
}

MBoundingBox CustomLocatorDrawOverride::boundingBox(const MDagPath& obPath, const MDagPath& cameraPath) const
{
	MPoint p1(-1.0f, -1.0f, -1.0f);
	MPoint p2(1.0f, 1.0f, 1.0f);
	return MBoundingBox(p1, p2);
}

bool CustomLocatorDrawOverride::hasUIDrawables() const
{
	return true;
}

MUserData* CustomLocatorDrawOverride::prepareForDraw(const MDagPath& obPath, const MDagPath& cameraPath, const MHWRender::MFrameContext& frameContext, MUserData* oldData)
{
	CustomLocatorData* userData = dynamic_cast<CustomLocatorData*>(oldData);
	if (!userData)
	{
		userData = new CustomLocatorData();
	}
	userData->drawIt = getPlugValue<bool>(obPath, CustomLocator::aDrawIt, true);
	if (!userData->drawIt) return userData;
	float alpha = getPlugValue<float>(obPath, CustomLocator::aAlpha, 1.0f);
	float line_width = getPlugValue<float>(obPath, CustomLocator::aLineWidth, 1.0f);

	bool useDistanceColor = getPlugValue<bool>(obPath, CustomLocator::aUseDistanceColor, false);
	if (useDistanceColor == true)
	{
		// Get Necessary Attributes only if necessary
		const double distanceAlpha = getPlugValue<double>(obPath, CustomLocator::aDistanceAlpha, 0.0f);
		const double minDistance = getPlugValue<double>(obPath, CustomLocator::aMinDistance, 0.0f);
		const double maxDistance = getPlugValue<double>(obPath, CustomLocator::aMaxDistance, 100.0f);
		// Get Locator World Matrix
		MPlug pWorldMatrix = MPlug(obPath.node(), CustomLocator::worldMatrix).elementByLogicalIndex(0);
		const MMatrix worldMatrix = MFnMatrixData(pWorldMatrix.asMObject()).matrix();

		// Calculate Distance from Camera to Locator and Lerp Alpha based on that Distance
		float distance = getDistanceFromCamera(worldMatrix, cameraPath);
		alpha = getClampedLerp(alpha, distanceAlpha, distance, minDistance, maxDistance);
	}

	userData->xRay = getPlugValue<bool>(obPath, CustomLocator::aXray, false);
	userData->billboard = getPlugValue<bool>(obPath, CustomLocator::aBillboard, false);
	const float scale = getPlugValue<float>(obPath, CustomLocator::aScale, 1.0f);
	const short shape = getPlugValue<short>(obPath, CustomLocator::aLocatorShape, 0);
	const float diskRadius = getPlugValue<float>(obPath, CustomLocator::aDiskRadius, 0.75f);
	const float coneRadius = getPlugValue<float>(obPath, CustomLocator::aConeRadius, 0.30f);

	bool rebuild = true;
	const DrawableType drawableType = static_cast<DrawableType>(shape);

	if (userData->drawable && drawableType == userData->drawable->getType())
	{
		rebuild = false;
	}

	switch (drawableType)
	{
	case DrawableType::Circle:
	{
		if (rebuild)
		{
			userData->drawable.reset(new Circle(scale, 16));
		}
		else
		{
			Circle* circle = static_cast<Circle*>(userData->drawable.get());
			circle->setParameters(scale, 16);
		}
		break;
	}

	case DrawableType::QuadArrow:
	{
		if (rebuild)
		{
			userData->drawable.reset(new QuadArrow(scale));
		}
		else
		{
			QuadArrow* quadArrow = static_cast<QuadArrow*>(userData->drawable.get());
			quadArrow->setParameters(scale);
		}
		break;
	}
	case DrawableType::Locator:
	{
		if (rebuild)
		{
			userData->drawable.reset(new Locator(scale));
		}
		else
		{
			Locator* locator = static_cast<Locator*>(userData->drawable.get());
			locator->setParameters(scale);
		}
		break;
	}
	case DrawableType::Disk:
	{
		if (rebuild)
		{
			userData->drawable.reset(new Disk(diskRadius * scale, scale, 16));
		}
		else
		{
			Disk* disk = static_cast<Disk*>(userData->drawable.get());
			disk->setParameters(diskRadius * scale, scale, 16);
		}
		break;
	}
	case DrawableType::Cone:
	{
		if (rebuild)
		{
			userData->drawable.reset(new Cone(coneRadius * scale, scale, 16));
		}
		else
		{
			Cone* cone = static_cast<Cone*>(userData->drawable.get());
			cone->setParameters(coneRadius * scale, scale, 16);
		}
		break;
	}
	case DrawableType::Cube:
	{
		if (rebuild)
		{
			userData->drawable.reset(new Cube(scale));
		}
		else
		{
			Cube* cube = static_cast<Cube*>(userData->drawable.get());
			cube->setParameters(scale);
		}
		break;
	}
	default:
		return userData;

	}

	float r, g, b;
	const auto displayStatus = MHWRender::MGeometryUtilities::displayStatus(obPath);
	if (displayStatus == MHWRender::kLead)
	{
		getRGBFromPlug(obPath.node(), CustomLocator::aLeadColor, r, g, b);
	}
	else if (displayStatus == MHWRender::kActive)
	{
		getRGBFromPlug(obPath.node(), CustomLocator::aSelectedColor, r, g, b);
	}
	else
	{
		getRGBFromPlug(obPath.node(), CustomLocator::aUnselectedColor, r, g, b);
	}
	userData->color = MColor(r, g, b, alpha);
	userData->line_width = line_width;
	return userData;
}

void CustomLocatorDrawOverride::addUIDrawables(const MDagPath& obPath, MHWRender::MUIDrawManager& drawManager, const MHWRender::MFrameContext& frameContext, const MUserData* data)
{
	const CustomLocatorData* userData = dynamic_cast<const CustomLocatorData*>(data);
	if (!userData || !userData->drawIt) {
		return;
	}
	drawManager.beginDrawable();
	if (userData->xRay) {
		drawManager.beginDrawInXray();
	}

	drawManager.setColor(userData->color);
	drawManager.setLineWidth(userData->line_width);

	MMatrix transform;
	if (userData->billboard)
	{
		MMatrix worldMatrix = obPath.inclusiveMatrix();
		MVector worldPos(worldMatrix[3][0], worldMatrix[3][1], worldMatrix[3][2]);
		MDagPath camDagPath = frameContext.getCurrentCameraPath();
		MMatrix camMatrix = camDagPath.inclusiveMatrix();
		MVector camWorldPos(camMatrix[3][0], camMatrix[3][1], camMatrix[3][2]);

		// Inverse of Eye direction
		MVector forward = camWorldPos - worldPos;
		forward.normalize();
		// Camera Up
		MVector up = MVector::yAxis * camMatrix;
		up.normalize();

		MVector right = up ^ forward;

		// It was identity
		transform.matrix[0][0] = forward.x;
		transform.matrix[0][1] = forward.y;
		transform.matrix[0][2] = forward.z;

		transform.matrix[1][0] = up.x;
		transform.matrix[1][1] = up.y;
		transform.matrix[1][2] = up.z;

		transform.matrix[2][0] = right.x;
		transform.matrix[2][1] = right.y;
		transform.matrix[2][2] = right.z;

		// Remove translation
		worldMatrix[3][0] = worldMatrix[3][1] = worldMatrix[3][2] = 0.0f;
		transform *= worldMatrix.inverse();
	}
	userData->drawable->draw(drawManager, transform);

	if (userData->xRay) {
		drawManager.endDrawInXray();
	}
	drawManager.endDrawable();
}

