#include "customLocator.h"

#include <maya/MGL.h> // OpenGL
#include <maya/MFnDependencyNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnMatrixData.h>
#include <maya/MHWGeometryUtilities.h>

#include "apiUtils.h"
#include "glUtils.h"

#include <memory>

MTypeId CustomLocator::typeId(0x001295c2);  // Registered for Minininja
MObject CustomLocator::aDrawIt;
MObject CustomLocator::aXray;
//MObject customLocator::aBackFaceCull; // Bool to Draw Back Faces
MObject CustomLocator::aBillboard;
//MObject customLocator::aWireframe; // Draw Wireframe
MObject CustomLocator::aLocatorShape;
MObject CustomLocator::aLineWidth; // Currently not used
MObject CustomLocator::aScale;
MObject CustomLocator::aAlpha;
MObject CustomLocator::aLeadColor; // Color when Lead
MObject CustomLocator::aSelectedColor; // Color when Selected
MObject CustomLocator::aUnselectedColor; // Color when Not Selected

MObject CustomLocator::aUseDistanceColor;
MObject CustomLocator::aMaxDistance; // Max Distance from Camera
MObject CustomLocator::aMinDistance; // Min Distance from Camera
MObject CustomLocator::aDistanceAlpha;

MObject CustomLocator::aConeRadius;
MObject CustomLocator::aDiskRadius;

// Viewport 2.0
MString CustomLocator::drawDbClassification("drawdb/geometry/customLocator");
MString CustomLocator::drawRegistrantId("customLocatorNodePlugin");

//// Thanks to Julien Caillaud
#define ADD_INPUT(attr, type) \
	type.setStorable(true); \
	type.setKeyable(true); \
	type.setWritable(true); \
	addAttribute(attr);

#define ADD_OUTPUT(attr, type) \
	type.setStorable(false); \
	type.setKeyable(false); \
	addAttribute(attr);

CustomLocator::CustomLocator()
{
	;
}

void CustomLocator::postConstructor()
{
	MObject object = thisMObject();
	MFnDependencyNode fnNode (object);
	fnNode.setName("customLocator#");
}

CustomLocator::~CustomLocator()
{
	;
}

void* CustomLocator::creator()
{
	return new CustomLocator();
}

MStatus CustomLocator::initialize()
{
	MFnNumericAttribute numAttr;
	MFnUnitAttribute unitAttr;
	MFnEnumAttribute enumAttr;

	aDrawIt = numAttr.create("drawIt", "drawIt", MFnNumericData::kBoolean, 1);
	ADD_INPUT(aDrawIt, numAttr);
	
	aXray = numAttr.create("xRay", "xRay", MFnNumericData::kBoolean, 0);
	ADD_INPUT(aXray, numAttr);

	aBillboard = numAttr.create("billboard", "billboard", MFnNumericData::kBoolean, 0);
	ADD_INPUT(aBillboard, numAttr);

	//aWireframe = numAttr.create("wireframe", "wireframe", MFnNumericData::kBoolean, 0);
	//ADD_INPUT(aWireframe, numAttr);

	//aBackFaceCull = numAttr.create("backFaceCull", "backFaceCull", MFnNumericData::kBoolean, 1);
	//ADD_INPUT(aBackFaceCull, numAttr);

	aLocatorShape = enumAttr.create("locatorShape", "locatorShape", static_cast<short>(DrawableType::Circle));
	enumAttr.addField("Circle", static_cast<short>(DrawableType::Circle));
	enumAttr.addField("Disk", static_cast<short>(DrawableType::Disk));
	enumAttr.addField("Cone", static_cast<short>(DrawableType::Cone));
	enumAttr.addField("Cube", static_cast<short>(DrawableType::Cube));
	enumAttr.addField("Locator", static_cast<short>(DrawableType::Locator));
	enumAttr.addField("QuadArrow", static_cast<short>(DrawableType::QuadArrow));
	ADD_INPUT(aLocatorShape, enumAttr);

	aLineWidth = numAttr.create("lineWidth", "lineWidth", MFnNumericData::kDouble, 1.0);
	numAttr.setMin(1.0f);
	ADD_INPUT(aLineWidth, numAttr);

	aScale = numAttr.create("scale", "scale", MFnNumericData::kDouble, 1.0);
	ADD_INPUT(aScale, numAttr);

	aAlpha = numAttr.create("alpha", "alpha", MFnNumericData::kDouble, 1.0);
	numAttr.setMin(0.0f);
	numAttr.setMax(1.0f);
	ADD_INPUT(aAlpha, numAttr);

	aLeadColor = numAttr.createColor("leadColor", "leadColor");
	numAttr.setDefault(1.0, 0.0, 0.0);
	ADD_INPUT(aLeadColor, numAttr);
	aSelectedColor = numAttr.createColor("selectedColor", "selectedColor");
	numAttr.setDefault(0.0, 1.0, 0.0);
	ADD_INPUT(aSelectedColor, numAttr);
	aUnselectedColor = numAttr.createColor("unselectedColor", "unselectedColor");
	numAttr.setDefault(0.5, 0.5, 0.5);
	ADD_INPUT(aUnselectedColor, numAttr);

	aUseDistanceColor = numAttr.create("useDistance", "useDistance", MFnNumericData::kBoolean);
	numAttr.setStorable(true);
	ADD_INPUT(aUseDistanceColor, numAttr);

	aMinDistance = numAttr.create("minDistance", "minDistance", MFnNumericData::kDouble, 0);
	numAttr.setStorable(true);
	ADD_INPUT(aMinDistance, numAttr);

	aMaxDistance = numAttr.create("maxDistance", "maxDistance", MFnNumericData::kDouble, 0);
	numAttr.setDefault(1000.0);
	ADD_INPUT(aMaxDistance, numAttr);

	aDistanceAlpha = numAttr.create("distanceAlpha", "distanceAlpha", MFnNumericData::kDouble, 0);
	numAttr.setMin(0.0);
	numAttr.setMax(1.0);
	numAttr.setDefault(0.0);
	ADD_INPUT(aDistanceAlpha, numAttr);

	aConeRadius = numAttr.create("coneRadius", "coneRadius", MFnNumericData::kDouble, 45.0f);
	numAttr.setMin(0.0f);
	numAttr.setMax(90.0f);
	numAttr.setDefault(45.0f);
	ADD_INPUT(aConeRadius, numAttr);

	aDiskRadius = numAttr.create("diskInner", "diskInner", MFnNumericData::kDouble, 0.75f);
	numAttr.setMin(0.0f);
	numAttr.setMax(1.0f);
	numAttr.setDefault(0.75f);
	ADD_INPUT(aDiskRadius, numAttr);

	return MS::kSuccess;
}

bool CustomLocator::isBounded() const
{
	return false;
}

bool CustomLocator::isTransparent() const
{
	return true;
}

MStatus CustomLocator::compute(const MPlug& plug, MDataBlock& data)
{
	return MS::kSuccess;
}

void CustomLocator::draw(M3dView& view, const MDagPath& dagPath, 
						 M3dView::DisplayStyle style, M3dView::DisplayStatus status)
{
	MPlug pDrawIt(thisMObject(), aDrawIt);
	bool drawIt;
	pDrawIt.getValue(drawIt);

	if (drawIt == false) {
		return;
	}

	MPlug pXray(thisMObject(), aXray);
	bool xRay;
	pXray.getValue(xRay);

	//MPlug pBackFaceCull(thisMObject(), aBackFaceCull);
	//bool backFaceCull;
	//pBackFaceCull.getValue(backFaceCull);

	MPlug pBillboard(thisMObject(), aBillboard);
	bool billboard;
	pBillboard.getValue(billboard);

	//MPlug pWireframe(thisMObject(), aWireframe);
	//bool wireframe;
	//pWireframe.getValue(wireframe);
		
	MPlug pLocatorShape(thisMObject(), aLocatorShape);
	short shape;
	pLocatorShape.getValue(shape);

	MPlug pSize(thisMObject(), aScale);
	double scale;
	pSize.getValue(scale);

	MPlug pAlpha (thisMObject(), aAlpha);
	double alpha;
	pAlpha.getValue(alpha);

	float selectedR, selectedG, selectedB;
	float leadR, leadG, leadB;
	float unselectedR, unselectedG, unselectedB;

	getRGBFromPlug(thisMObject(), aSelectedColor, selectedR, selectedG, selectedB);
	getRGBFromPlug(thisMObject(), aUnselectedColor, unselectedR, unselectedG, unselectedB);
	getRGBFromPlug(thisMObject(), aLeadColor, leadR, leadG, leadB);

	MPlug pUseDistanceColor(thisMObject(), aUseDistanceColor);
	bool useDistanceColor;
	pUseDistanceColor.getValue(useDistanceColor);

	if (useDistanceColor == true)
	{
		// Get Necessary Attributes only if necessary
		MPlug pDistanceAlpha(thisMObject(), aDistanceAlpha);
		double distanceAlpha;
		pDistanceAlpha.getValue(distanceAlpha);

		MPlug pMinDistance(thisMObject(), aMinDistance);
		double minDistance;
		pMinDistance.getValue(minDistance);

		MPlug pMaxDistance(thisMObject(), aMaxDistance);
		double maxDistance;
		pMaxDistance.getValue(maxDistance);

		// Get Locator World Matrix
		MPlug pWorldMatrix = MPlug(thisMObject(), worldMatrix).elementByLogicalIndex(0);
		MMatrix worldMatrix = MFnMatrixData(pWorldMatrix.asMObject()).matrix();

		// Calculate Distance from Camera to Locator and Lerp Alpha based on that Distance
		float distance = getDistanceFromCamera(worldMatrix, view);
		alpha = getClampedLerp(alpha, distanceAlpha, distance, minDistance, maxDistance);
	}


	/* Initialize OpenGL and Draw */
	view.beginGL();
	glPushAttrib(GL_ALL_ATTRIB_BITS);

	glPushMatrix();
	// If Billboard
	if (billboard == true) {
		MMatrix worldMatrix = dagPath.inclusiveMatrix() ;

		// Remove translate
		worldMatrix[3][0] = 0.0;
		worldMatrix[3][1] = 0.0;
		worldMatrix[3][2] = 0.0;

		// Get Camera Matrix
		MDagPath camDagPath;
		view.getCamera(camDagPath);
		MMatrix camMatrix = camDagPath.inclusiveMatrix();

		// Remove Translate
		camMatrix[3][0] = 0.0;
		camMatrix[3][1] = 0.0;
		camMatrix[3][2] = 0.0;

		MMatrix m = camMatrix * worldMatrix.inverse();
		GLfloat billboardMatrix[16] = {
			m[0][0], m[0][1], m[0][2], m[0][3],
			m[1][0], m[1][1], m[1][2], m[1][3],
			m[2][0], m[2][1], m[2][2], m[2][3],
			m[3][0], m[3][1], m[3][2], m[3][3]
		};
		glMultMatrixf(billboardMatrix);
		// As all the primitives we are drawing are pointing to X we are going to Rotate
		glRotatef(-90, 0, 1, 0);
	}

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// XRAY
	// http://christopherlewisblog.blogspot.com.es/2011/07/xray-joints-with-maya-python-api.html
	// Override Depth Buffer
	if (xRay == true) {
		glClearDepth(0.0);
		glDepthFunc(GL_ALWAYS);
	}

	// Wireframe and Back Face Culling
	//if (wireframe == true) {
	//	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	//}
	//else if(backFaceCull == true) {
	//	glEnable(GL_CULL_FACE);
	//	glCullFace(GL_BACK);
	//}

	// Define the Color
	if (status == M3dView::kLead)
	{
		glColor4f(leadR, leadG, leadB, alpha);
	}
	else if(status == M3dView::kActive)
	{
		glColor4f(selectedR, selectedG, selectedB, alpha);
	}
	else
	{
		glColor4f(unselectedR, unselectedG, unselectedB, alpha);
	}

	DrawableType drawableType = static_cast<DrawableType>(shape);
	// Define the Shape
	switch (drawableType)
	{
	case DrawableType::Circle:
	{
		circle(scale, 16);
		break;
	}
	case DrawableType::Cube:
	{
		cube(scale);
		break;
	}
	case DrawableType::Disk:
	{
		MPlug pDiskInner(thisMObject(), aDiskRadius);
		float inner;
		pDiskInner.getValue(inner);
		disk(inner * scale, 1.0f * scale, 16);
		break;
	}
	case DrawableType::Locator:
	{
		locator(scale);
		break;
	}
	case DrawableType::QuadArrow:
	{
		quadArrow(scale);
		break;
	}
	case DrawableType::Cone:
	{
		MPlug pConeRadius(thisMObject(), aConeRadius);
		float coneRadius;
		pConeRadius.getValue(coneRadius);
		cone(coneRadius, scale, 16);
		break;
	}
	}
	
	glDisable(GL_BLEND);
	glPopMatrix(); // Restore Matrix
	glPopAttrib(); // Restore Attributes
	view.endGL();
}
