//
// Copyright (C) Gorka Mendieta Moreno 
// 
// File: pluginMain.cpp
//
// Author: Maya Plug-in Wizard 2.0
//

#include "customLocator.h"
#include "customLocatorDrawOverride.h"

#include <maya/MFnPlugin.h>
#include <maya/MDrawRegistry.h>

MStatus initializePlugin( MObject ob )
{ 
	MStatus   status;
	MFnPlugin fnPlugin( ob, "Gorka Mendieta Moreno", "2016", "Any");

	status = fnPlugin.registerNode("customLocator",
		CustomLocator::typeId,
		CustomLocator::creator,
		CustomLocator::initialize,
		MPxNode::kLocatorNode,
		&CustomLocator::drawDbClassification
		);
	if (!status)
	{
		status.perror("Failed to register customLocator node");
		return status;
	}

	status = MHWRender::MDrawRegistry::registerDrawOverrideCreator(
		CustomLocator::drawDbClassification,
		CustomLocator::drawRegistrantId,
		CustomLocatorDrawOverride::creator
		);
	if (!status)
	{
		status.perror("Failed to register customLocator drawOverride");
		return status;
	}
	
	return status;
}

MStatus uninitializePlugin( MObject ob )
{
	MStatus   status;
	MFnPlugin fnPlugin( ob );

	status = fnPlugin.deregisterNode(CustomLocator::typeId);
	CHECK_MSTATUS_AND_RETURN_IT(status);

	status = MHWRender::MDrawRegistry::deregisterDrawOverrideCreator(
		CustomLocator::drawDbClassification,
		CustomLocator::drawRegistrantId
	);
	CHECK_MSTATUS_AND_RETURN_IT(status);
	return status;
}
